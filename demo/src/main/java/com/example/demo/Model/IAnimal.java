package com.example.demo.Model;

public interface IAnimal {
    public void animalSound();
    public void animalSleep();
}
